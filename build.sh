#!/bin/bash

set -e

sitetitle="Path.CBUlt.space"

# setup
mkdir -p ./public

function build() {
  outputpath="./public/${1%.md}/index.html"
  if [ "$1" = "index.md" ]; then
    outputpath="./public/index.html"
  else
    mkdir -p "./public/${1%.md}"
  fi

  pandoc "./content/$1" --from="markdown+ignore_line_breaks" --standalone --template=layout/basic.html --variable=title-suffix:"$sitetitle" --output="$outputpath"
}

for mdfile in content/*.md
do
  build "${mdfile#content/}"
done
