# Path

## Prerequisites

- [pandoc](https://pandoc.org) 3.1.9

## Build

```
$ ./build.sh
```

Or you can use [watchexec](https://github.com/watchexec/watchexec).

```
$ watchexec -e md,html ./build.sh
```

## Preview

```
$ ./server.sh
```
