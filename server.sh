#!/bin/bash

set -e

port=7890

cd public
echo "http://localhost:$port"
python3 -m http.server $port
